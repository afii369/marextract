# marextract

A small Python script that extracts proprietary MSN Explorer .MAR files.

## How to run

If needed, place the `marextract.py` script in any directory you want and set it's location in the `PATH` environment variable for global use.

For in-app help, run the script with the `--help` argument or no arguments at all.

To extract a .MAR file, run the script with the .MAR file you want to extract as the first argument.