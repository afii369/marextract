import sys
import os
import struct
import binascii

help_msg = '''
marextract.py - Extracts proprietary MSN Explorer .MAR files
Syntax: marextract.py [--help] mar_file

Arguments:

--help - Display the help prompt.
mar_file - The specified .MAR file you want to extract.'''

def main():
	# Handle the arguments
	
	try:
		arg1 = sys.argv[1]
	except IndexError:
		arg1 = None
	
	if arg1 in ('--help',None):
		print(help_msg)
		sys.exit(-1)
	
	mar = arg1
	
	# Make sure the specified file ends with '.mar'
	
	if not mar.endswith('.mar'):
		print('File specified is not a .MAR file!')
		sys.exit(-1)
	
	try:
		with open(mar, 'rb') as mar_file:
			contents = mar_file.read()
	except FileNotFoundError:
		print('File', '"' + mar + '"', 'does not exist. Cancelling!')
		sys.exit(-1)
	
	# Check for magic value
	
	if not contents.startswith(b'MARC\x03\x00\x00\x00'):
		print('Specified file is not a valid .MAR. Terminating...')
		sys.exit(-1)
	
	# Retreive the number of files
	
	num_files = struct.unpack('<I', contents[8:12])[0]
	file_struct_loc = 12
	for _ in range(0, num_files):
		# Retreive file info, like the file path and filename, the file's size, CRC32, and position within the .MAR
		
		filename = contents[file_struct_loc:(file_struct_loc + 56)].replace(b'\x00', b'').decode('utf-8')
		file_struct_loc += 56
		size = struct.unpack('<I', contents[file_struct_loc:(file_struct_loc + 4)])[0]
		file_struct_loc += 4
		crc32 = contents[file_struct_loc:(file_struct_loc + 4)]
		file_struct_loc += 4
		file_pos = struct.unpack('<I', contents[file_struct_loc:(file_struct_loc + 4)])[0]
		file_struct_loc += 4
		
		# With the file's position within the .MAR retreived, grab the data
		
		file_data = contents[file_pos:(file_pos + size)]
		
		# Now do a CRC32 check
		
		our_crc32 = struct.pack('<I', (binascii.crc32(file_data) % (1 << 32)))
		if our_crc32 != crc32:
			print('.MAR archive entry contains invalid CRC32. Terminating...')
			sys.exit(-1)
		
		# Set up a folder to where the .MAR's files will be extracted to
		
		file_store = mar[:(len(mar) - 4)]
		
		if not os.path.exists(file_store):
			os.makedirs(file_store)
		
		filename_path = file_store
		subpaths = '/' + '/'.join(filename.split('/')[:-1])
		if len(subpaths) > 1: filename_path += subpaths
		filename_base = filename.split('/').pop()
		
		if not os.path.exists(filename_path):
			os.makedirs(filename_path)
		
		# Finally, write data to the extracted file
		
		with open(filename_path + '/' + filename_base, 'wb') as file:
			file.write(file_data)
			file.close()
	
	print('.MAR archive extraction successful!')

if __name__ == '__main__':
	main()